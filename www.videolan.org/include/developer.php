<?php
function isDeveloper() {
    return (php_sapi_name() == 'cli-server');
    //comment previous entry to be in developer mode
    return true;
}

function skipDatabase() {
    return (isset($_ENV['SKIP_DATABASE']) && !empty($_ENV['SKIP_DATABASE'])) || (isset($_SERVER['SKIP_DATABASE']) && !empty($_SERVER['SKIP_DATABASE']));
}

?>

<?php

$alternate_lang = array();

if ( $handle = opendir( $_SERVER["DOCUMENT_ROOT"]."/locale/" ) ) {
    while ( false !== ( $file = readdir( $handle ) ) ) {
        if ( $file === "." ) { continue; }
        if ( $file === ".." ) { continue; }
        if ( is_dir( $_SERVER["DOCUMENT_ROOT"]."/locale/".$file ) ) {
            array_push( $alternate_lang, $file );
        }
    }
    closedir($handle);
}

sort( $alternate_lang );

?>

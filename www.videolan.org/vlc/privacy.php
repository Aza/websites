<?php
   $title = 'Legal Privacy';
   $body_color = "green";
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>

<div>
<h1>Privacy</h1>

<h2>VLC Mobile</h2>
<p>
This privacy policy covers the use of the 'VLC for Android', 'VLC for WinRT' and 'VLC for iOS' / 'VLC for tvOS' applications.<br />
It may not be applicable to other software produced or released by VideoLAN.
</p>
<p>
VideoLAN does not collect any statistics, personal information, or analytics from our users, other than built in mechanisms that are present for all the mobile or embedded applications in their respective main distribution channels.
</p>
<p>
The mobile or embedded versions of 'VLC' do allow for videos to be played via various network transports. Cookies are not stored at any point. Authentication credentials can be stored optionally on the user's local device upon the user's explicit request. On iOS and tvOS, authentication credentials can be shared across devices using proprietary encryption mechanisms provided by the host operating system, dubbed iCloud Keychain. VideoLAN does not have access to any information stored within.
</p>
<p>The 'VLC for iOS' / 'VLC media player' app on iOS and iPadOS allows users to optionally log in to their personal Google Drive accounts. This enables the playback of media files stored in the user's Google Drive and allows the download of media to the iOS or iPadOS device. The login is optional, and the app remains fully functional without this feature. All media downloads or playback are initiated by the user. The app never has access to user credentials, as the login process is handled externally through the OAuth2 protocol. The resulting session token is encrypted and stored securely in the keychain provided by iOS and iPadOS. No user data, including login credentials, session tokens, or contact information, is ever shared with the VideoLAN non-profit organization. Media files downloaded at the user's explicit request are stored locally on the device and are subject to the security measures of iOS and iPadOS.
</p>

</div>

<?php footer();?>

<?php
    $title = "libVLC media player, Open Source video framework for every OS!";
    $lang = "en";
    $new_design = true;
    require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
    require($_SERVER["DOCUMENT_ROOT"]."/include/os-specific.php");
?>

<div class="container">
    <h1 class="bigtitle">libVLC</h1>

    <p><b>libVLC</b> is the core engine and the interface to the multimedia framework
    on which <a href="/vlc/">VLC media player</a> is based.</p>
    <p><b>libVLC</b> is modularized into hundreds of plugins, which may be loaded at runtime. 
    This architecture provides great flexibility to developers (both VLC devs and devs consuming the library). 
    It allows developers to create a wide range of multimedia applications
    using the <a href="/vlc/">VLC</a> <a href="/vlc/features.html">features</a>.</p>
    <ul class="bullets">
        <li>Play every media file formats, every codec and every streaming protocols.</li>
        <li>Run on every platform, from desktop (Windows, Linux, Mac) to mobile (Android, iOS) and TVs.</li>
        <li>Hardware and efficient decoding on every platform, up to 8K.</li>
        <li>Network browsing for distant filesystems (SMB, FTP, SFTP, NFS...) and servers (UPnP, DLNA).</li>
        <li>Playback of Audio CD, DVD and Bluray with menu navigation.</li>
        <li>Support for HDR, including tonemapping for SDR streams.</li>
        <li>Audio passthrough with SPDIF and HDMI, including for Audio HD codecs, like DD+, TrueHD or DTS-HD.</li>
        <li>Support for video and audio filters.</li>
        <li>Support for 360 video and 3D audio playback, including Ambisonics.</li>
        <li>Able to cast and stream to distant renderers, like Chromecast and UPnP renderers.</li>
    </ul>

    <p><b>libVLC</b> is a C library which can be embedded in your own applications. It works with most popular OS platforms, on both mobile and desktop.
    It is under the <b>LGPL2.1 license.</b></p>

    <p><b>libVLC</b> versioning is inherently tied to the VLC app versioning. The current stable major version of libVLC is version <b>3</b>, 
    and the preview/development version is version <b>4</b>.</p>
    
    <p>A variety of programming language bindings for libVLC is available to use the library seamlessly in your chosen ecosystem.</p>
    <h2>Bindings by VideoLAN</h1>
    <ul class="bullets">
        <li><a href="https://code.videolan.org/videolan/libvlcpp">libvlcpp</a> for C++</li>
        <li><a href="https://code.videolan.org/videolan/VLCKit">VLCKit</a> for Apple platforms, using Objective-C/Swift.</li>
        <li><a href="https://code.videolan.org/videolan/vlc-android/-/tree/master/libvlc">libvlcjni</a> for Android platforms, using Java/Kotlin.</li>
        <li><a href="https://code.videolan.org/videolan/LibVLCSharp">LibVLCSharp</a> for most OS platforms, using .NET/Mono.</li>
    </ul>
    <h2>Bindings by the community</h1>
    <ul class="bullets">
        <li><a href="https://github.com/caprica/vlcj">vlcj</a> for Desktop plaforms using Java.</li>
        <li><a href="https://github.com/oaubert/python-vlc">python-vlc</a> for Desktop platforms using Python.</li>
        <li><a href="https://github.com/garkimasera/vlc-rs">vlc-rs</a> using the Rust programming language.</li>
        <li><a href="https://github.com/adrg/libvlc-go">libvlc-go</a> using the Go programming language.</li>
    </ul>
    <h2>LibVLC Discord Community Server</h2>    
    <p>For matters related to the LibVLC APIs and the various bindings, join our LibVLC bindings Community Discord Server!</p>
    <a href="https://discord.gg/3h3K3JF">
        <img src='https://img.shields.io/discord/716939396464508958?label=discord'/>
    </a>
    <h1>LibVLC Examples</h1>
    <p><b>Sample</b> projects using LibVLC made by the community are a great way to get started!</p>
    
    <p>You can just clone and run the sample and start from there. It is also a good way to <i>learn how to implement and use specific LibVLC features</i> in your applications. <b>Test projects</b> are also good for this.</p>
    </br>
    <figure style="text-align: center;">
        <img style="max-width: 100%" src="//images.videolan.org/images/mosaic-android.png" Width="400" alt="mosaic android" />
        <figcaption><i>Video player with Mosaic views on Android</i></figcaption>
    </figure> 
    </br>
    <figure style="text-align: center;">
        <a href="https://player.vimeo.com/video/254723180"><img style="max-width: 100%" src="//images.videolan.org/images/360-video.png" Width="400" alt="mosaic android" /></a>
        <figcaption><i>360° video and viewpoint navigation</i></figcaption>
    </figure> 
    </br>
    <figure style="text-align: center;">
        <img style="max-width: 100%" src="//images.videolan.org/images/thumbnailer.jpg" Width="400" alt="thumbnailer" /></a>
        <figcaption><i>LibVLC Thumbnailer output</i></figcaption>
    </figure> 
    </br>
    <figure style="text-align: center;">
        <img style="max-width: 100%" src="//images.videolan.org/images/mediaplayerelement.png" Width="400" alt="mediaplayerelement" /></a>
        <figcaption><i>Crossplatform MediaPlayerElement from LibVLCSharp</i></figcaption>
    </figure> 

    <h2>Samples repositories for each ecosystem</h2>

    <p>You can find in the following list links to ready-to-run code samples using the various LibVLC bindings.</p>
    <ul class="bullets">
        <li><a href="https://code.videolan.org/mfkl/libvlcsharp-samples">LibVLCSharp samples</a></li>
        <li><a href="https://github.com/caprica/vlcj-examples/tree/master/src/main/java/uk/co/caprica/vlcj/test">vlcj samples</a></li>
        <li><a href="https://code.videolan.org/videolan/libvlcpp/-/blob/master/test/main.cpp">libvlcpp tests</a></li>
        <li><a href="https://code.videolan.org/videolan/VLCKit/-/tree/master/Examples">VLCKit samples</a></li>
        <li><a href="https://code.videolan.org/videolan/libvlc-android-samples">libvlcjni samples</a></li>
        <li><a href="https://github.com/oaubert/python-vlc/tree/master/examples">python-vlc samples</a></li>
    </ul>

    <h2>Ebook: The Good Parts of LibVLC</h2>

    <p>
        <b><a href="https://mfkl.gumroad.com/l/libvlc-good-parts">The Good Parts of LibVLC</a></b> is the first book ever about LibVLC and the VideoLAN community. It presents the VideoLAN non-profit organization and dives deep into the LibVLC native library for developers and its usage on most platforms. 
        
        This is a technical ebook for programmers and consultants who want to learn more about the LibVLC SDK. The ebook was published in September 2022.
    </p>

    <figure style="text-align: center;">
        <a href="https://mfkl.gumroad.com/l/libvlc-good-parts"><img style="max-width: 100%" src="//images.videolan.org/images/thegoodpartsoflibvlc.png" Width="200" alt="libVLC ebook" /></a>
    </figure>

    <h2>Technical Diagram</h2>
    <a href="//images.videolan.org/images/libvlc_stack.png">
        <img style="max-width: 100%" src="//images.videolan.org/images/libvlc_stack.png" alt="libVLC dev stack" />
    </a>

</div>

<?php footer(); ?>


<?php
   $title = "Video Dev Days 2018, September 21 - 23, 2018";
   $body_color = "orange";

   $new_design = true;
   $show_vdd_banner = false;
   $lang = "en";
   $menu = array( "videolan", "events" );

   $additional_js = array("/js/slimbox2.js", "/js/slick-init.js", "/js/slick.min.js");
   $additional_css = array("/js/css/slimbox2.css", "/style/slick.min.css", "/videolan/events/vdd18/style.css");
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>
<script>
  countdownManager = {
    targetTime: new Date(),
    element: {
        day: null,
        hour: null,
        min: null,
        sec: null
    },
    interval: null,
    init: function(targetTime) {
        this.targetTime = targetTime || new Date();
        this.element.day  = $('#countdown-day');
        this.element.hour = $('#countdown-hour');
        this.element.min  = $('#countdown-min');
        this.element.sec  = $('#countdown-sec');

        this.tick();
        this.interval = setInterval('countdownManager.tick();', 1000);
    },

    tick: function() {
        var timeNow = new Date();
        if (timeNow > this.targetTime) {
            timeNow = this.targetTime;
            $('#countdown').hide();
            clearInterval(this.interval);
            return;
        }

        var diff = this.dateDiff(timeNow, this.targetTime);

        this.element.day.text(diff.day);
        this.element.hour.text(diff.hour);
        this.element.min.text(diff.min);
        this.element.sec.text(diff.sec);
    },

    dateDiff: function(date1, date2) {
        var diff = {};
        var tmp = date2 - date1;

        tmp = Math.floor(tmp / 1000);
        diff.sec = tmp % 60;
        tmp = Math.floor((tmp-diff.sec) / 60);
        diff.min = tmp % 60;
        tmp = Math.floor((tmp-diff.min) / 60);
        diff.hour = tmp % 24;
        tmp = Math.floor((tmp-diff.hour) / 24);
        diff.day = tmp;
        return diff;
    }
};

$(function($){
    countdownManager.init(new Date('2018-09-21'));
});

</script>
  <div class="sponsor-box-2">
    <h4>Sponsors</h4>
    <a href="https://www.google.com" target="_blank">
        <?php image( 'events/vdd18/sponsors/google-logo.png' , 'Google', 'sponsors-logo'); ?>
    </a>
    <a href="https://www.mozzila.org" target="_blank">
        <?php image( 'events/vdd18/sponsors/mozilla-logo.png', 'Mozilla', 'sponsors-logo'); ?>
    </a>
  </div>
<header class="header-bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2 text-center">
        <img src="//images.videolan.org/images/VLC-IconSmall.png">
        <h1>Video Dev Days 2018</h1>
        <h3>The Open Multimedia Conference that frees the cone in you!</h3>
        <h4>21 - 23 September 2018, Paris</h4>
        <a href="https://goo.gl/forms/8WGREy2hMxRNfGzv2" class="btn btn-border vbtn-link">Register<span class="arrow right vdd-icon"></span></a>
      </div>
    </div>
    <div class="row">
      <div id="countdown">
        <div class="countdown-box">
          <span id="countdown-day" >--</span>
          <div class="countdown-unit">Days</div>
        </div>
        <div class="countdown-box">
          <span id="countdown-hour">--</span>
          <div class="countdown-unit">Hours</div>
        </div>
        <div class="countdown-box">
          <span id="countdown-min" >--</span>
          <div class="countdown-unit">Minutes</div>
        </div>
        <div class="countdown-box">
          <span id="countdown-sec" >--</span>
          <div class="countdown-unit">Seconds</div>
        </div>
      </div>
    </div>
  </div>
  <!-- <div class="container">
    <div id="sponsors">
        <h5>Sponsors</h5>
        <p>We are looking for sponsors</p>
    </div>
  </div> -->
</header>
<section id="overview">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 col-lg-offset-1 text-center">
        <h2 class="uppercase">
          Video Dev Days 2018
          <span class="spacer-inline">About</span>
        </h2>
        <p>The <a href="/videolan/">VideoLAN non-profit organisation</a> is happy to
        invite you to the multimedia open-source event of the summer!</p>
        <p>For its <b>tenth edition</b>, people from the VideoLAN and open source multimedia communities will meet in <strong>Paris</strong>
        to discuss and work on the future of the open-source multimedia community</p>

        <p>This is a <strong>technical</strong> conference.</p>
        <div class="row">
          <div class="col-md-6">
            <div class="text-box when-box">
              <h4 class="text-box-title">When?</h4>
              <p class="text-box-content">21st - 23rd of September 2018</p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="text-box where-box">
              <h4 class="text-box-title">Where?</h4>
              <p class="text-box-content">Paris</p>
            </div>
          </div>
        </div>



      </div>
    </div>
  </div>
</section>
<!-- <section id="where" class="bg-gray">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 col-lg-offset-1 text-center">
        <h2 class="uppercase">Where?</h2>
        <p>The venue is in Paris!</p>
      </div>
    </div>
  </div>
</section> -->

<section id="schedule">
  <div class="container">
    <div class="row">
      <div class="text-center">
        <h2 class="uppercase">Schedule</h2>
        <hr class="spacer-2">
      </div>
      <div class="col-lg-10 col-lg-offset-1">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation"><a href="#friday" aria-controls="friday" role="tab" data-toggle="tab">Friday 21</a></li>
        <li role="presentation" class="active"><a href="#saturday" aria-controls="saturday" role="tab" data-toggle="tab">Saturday 22</a></li>
        <li role="presentation"><a href="#sunday" aria-controls="sunday" role="tab" data-toggle="tab">Sunday 23</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade" id="friday">
            <div class="event">
              <h4 class="event-time">09:30 - 18:30</h4>
              <div class="event-description">
                <h3>Community Bonding Day: Visit Paris!</h3>
                <p>This year we'll do a <b>Visit Paris competition</b>!<br/>
                The VideoLAN organization will pay for whatever costs associated with the event.<br/></p>
              </div>
            </div>
            <div class="event">
              <h4 class="event-time">19:30</h4>
              <div class="event-description">
                <h3>Evening drinks</h3>
                <p>On <strong>Friday at 19h30</strong>, people are welcome to come and
                share a few good drinks, with all attendees, at the <a href="https://goo.gl/maps/tTTAPwjzHe62" target="_blank">King George pub</a>.</p>
                <p>Alcoholic and non-alcoholic drinks will be available!</b>
              </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade active in" id="saturday">
            <div class="event event-breakfast">
              <h4 class="event-time">
                08:30 - 09:00
              </h4>
              <div class="event-description">
                <h3>Registration & Breakfast</h3>
              </div>
            </div>

            <div class="event event-welcome">
              <h4 class="event-time">
                09:00 - 09:25
              </h4>
              <p class="event-author">
                <span class="avatar avatar-jb"></span>Jean-Baptiste Kempf, VideoLAN
              </p>
              <div class="event-description">
                <h3>Welcome</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                09:25 - 09:40
              </h4>
              <p class="event-author">
                <span class="avatar avatar-pm"></span>Pascal Massimino, Google
              </p>
              <div class="event-description">
                <h3>AV1: in the end, what got in?</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                09:40 - 10:05
              </h4>
              <p class="event-author">
                <span>
                  <span class="person-block"><span class="avatar avatar-jb"></span>Jean-Baptiste Kempf</span>
                  <span class="avatar avatar-rb"></span>Ronald Bultje
                </span>
              </p>
              <div class="event-description">
                <h3>Dav1d: a fast new AV1 decoder</h3>
                <p>Dav1d is Dav1d.</p>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                10:05 - 10:30
              </h4>
              <p class="event-author">
                <span>
                  <span class="avatar avatar-td"></span>Thomas Daede, Mozilla
                </span>
              </p>
              <div class="event-description">
                <h3>rav1e: the best rust AV1 encoder</h3>
              </div>
            </div>

            <div class="event event-coffee">
              <h4 class="event-time">
                10:30 - 10:50
              </h4>
              <div class="event-description">
                <h3>Coffee Break</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                10:50 - 11:20
              </h4>
              <p class="event-author"><span class="avatar avatar-rp"></span>Rostislav Pehlivanov, FFmpeg</p>
              <div class="event-description">
                <h3>FFv2</h3>
                <p>A technical talk about FFv2, and directions for the future.</p>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                11:30 - 12:00
              </h4>
              <p class="event-author"><span class="avatar avatar-pr"></span>Pradeep Ramachandran</p>
              <div class="event-description">
                <h3>x265: an update</h3>
                <p>x265: what happened last year.</p>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                12:00 - 12:30
              </h4>
              <p class="event-author"><span class="avatar avatar-jb"></span>Jean-Baptiste Kempf, VideoLAN</p>
              <div class="event-description">
                <h3>VLC 4.0</h3>
                <p>VLC 4.0: what was planned, what is being done, and what we will finally get!<br />VR!</p>
              </div>
            </div>

            <div class="event event-lunch">
              <h4 class="event-time">
                12:30 - 14:00
              </h4>
              <div class="event-description">
                <h3>Lunch Break</h3>
              </div>
            </div>

            <div class="event event-meetups">
              <h4 class="event-time">
                14:00 - 18:00
              </h4>
              <div class="event-description">
                <h3>Meetups</h3>
              </div>
            </div>

            <div class="event">
              <h4 class="event-time">
                19:30 - ??:??
              </h4>
              <div class="event-description">
                <h3>Community Dinner</h3>
                <p> Location: Port de Javel Haut</p>
              </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane fade" id="sunday">
            <div class="event event-breakfast">
              <h4 class="event-time">
                09:00 - 09:30
              </h4>
              <div class="event-description">
                <h3>Breakfast</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                09:30 - 12:00
              </h4>
              <div class="event-description">
                <h3>Lightning talks</h3>
                <ul>
                  <li>An update about LLVM + Mingw (<i>Martin Storsjo</i>)</li>
                  <li>Rust & Multimedia (<i>Luca Barbato</i>)</li>
                  <li>TTML subtitles (<i>Stefan Pöschel</i>)</li>
                  <li>HDR & placebo (<i>Niklas Haas</i>)</li>
                  <li>FFv1 update (<i>Dave Rice</i>)</li>
                  <li>Low-latency Dash streaming (<i>Romain Bouqueau</i>)</li>
                  <li>Allwiner Cedrus driver and VPU (<i>Paul Kocialkowski</i>)</li>
                  <li>Bro Talk Translation (<i>Derek Buitenhuis</i>)</li>
                  <li>VLC multi-process (<i>Alexandre Janni</i>)</li>
                  <li>Intel and FFmpeg (<i>Vasily Aristarkhov</i>)</li>
                  <li>SubExponential Golomb Codes in AV1 (<i>Luc Trudeau</i>)</li>
                </ul>
              </div>
            </div>

            <div class="event event-bg event-lunch">
              <h4 class="event-time">
                12:00 - 14:00
              </h4>
              <div class="event-description">
                <h3>Lunch</h3>
              </div>
            </div>

            <div class="event">
              <h4 class="event-time">
                14:00 - 18:00
              </h4>
              <div class="event-description">
                <h3>Unconferences</h3>
              </div>
            </div>

            <div class="event">
              <div class="event-inner">
                <div class="event-description">
                    <p>The actual content of the unconference track is being decided on Saturday evening. For the live schedule, check the <a href="https://wiki.videolan.org/VDD18#Unconference_schedule">designated page on the wiki</a>.</p>
                </div>
              </div>
            </div>

            <div class="event">
              <h4 class="event-time">
                20:00 - ??:??
              </h4>
              <div class="event-description">
                <h3>Unofficial Dinner</h3>
                <p>Karaoke + Fishing?</p>
              </div>
            </div>
        </div>
      </div>
      </div>
    </div>
  </div>
</section>

<section id="who-come">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 text-box" id="who-come-box">
        <h2 class="uppercase">Who can come?
        </h2>
        <p><strong>Anyone</strong> who cares about open source multimedia technologies and development. Remember that it targets a technical crowd!</p>
        <p>If you are representing a <b>company</b> caring about open-source multimedia software, we would be <b>very interested</b> if you could co-sponsor the event.</p>
      </div>

    </div>
  </div>
</section>

<section id="register-section">
  <div class="container">
      <h2 class="uppercase">Register</h2>
      <div class="row">
        <div class="col-md-6">
          <div class="text-box register-box">
            <h3 class="text-box-title">
              COST AND SPONSORSHIP
            </h3>
            <h3>FREE</h3>
            <p class="text-box-content">
              The cost for attendance is free.

              Like previous years, active developers can get a full sponsorship covering travel costs. We will also provide accomodation.
            </p>
            <div class="ticket">
              Register <a href="https://goo.gl/forms/8WGREy2hMxRNfGzv2">HERE!</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="text-box register-box">
            <h3 class="text-box-title">Accommodation</h3>
            <p>For active members of the open-source multimedia communities who registered until August 25 and got a confirmation, your hotel will be one of the following:</p>
            <ul>
              <li>Hotel Monsieur, 62 rue des Mathurins, Paris 8
              <li>Mercure Paris Opéra Faubourg Montmartre, 5 Rue De Montyon, Paris 9
            </ul>
          </b></p>
          </div>
        </div>
      </div>
  </div>
</section>
<section>
  <div class="container">
    <h2 class="text-center uppercase">Sponsors</h2>
    <hr class="spacer-2">

    <p class="big-p text-center">
      We are looking for sponsors
    </p>
    <section class="text-center">
      <a href="https://www.google.com" target="_blank">
        <?php image( 'events/vdd18/sponsors/google-logo.png' , 'Google', 'sponsors-logo-2'); ?>
      </a>
      <br/>
      <a href="https://www.mozilla.org" target="_blank">
        <?php image( 'events/vdd18/sponsors/mozilla-logo.png' , 'Mozilla', 'sponsors-logo-2'); ?>
      </a>
    </section>
  </div>
</section>

<section>
  <div class="container">
    <h2 class="text-center uppercase">Locations</h2>
    <hr class="spacer-2">
    <div class="row">
      <div class="col-md-6">
        <div class="text-box conference-box">
          <div class="text-box-title">
            Conference
          </div>
          <div class="text-box-content">
            Free.Fr/Iliad HQ
            16, Rue de la Ville-l'Évêque
            75008 Paris
            France
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="text-box dinner-box">
          <div class="text-box-title">
              Saturday Dinner
          </div>
          <div class="text-box-content">
              Port de Javel Haut, Paris
          </div>
        </div>
      </div>
    </div>

  </div>
</section>


<section>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2 class="uppercase">Code of Conduct </h2>
        <p>This community activity is running under the <a href="https://wiki.videolan.org/CoC/">VideoLAN Code of Conduct</a>. We expect all attendees to respect our <a href="https://wiki.videolan.org/VideoLAN_Values/">Shared Values</a>.</p>
      </div>
      <div class="col-md-6">
        <h2 class="uppercase">Contact </h2>
        <p>The VideoLAN Dev Days are organized by the board members of the VideoLAN non-profit organization, Jean-Baptiste Kempf, Denis Charmet, Konstantin Pavlov and Hugo Beauz&eacute;e-Luyssen. You can reach us at <span style="color: #39b549">board@videolan.org</span>.</p>
      </div>
    </div>
  </div>
</section>



<?php footer('$Id: index.php 5400 2009-07-19 15:37:21Z jb $'); ?>

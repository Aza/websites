<?php
   $title = "VideoLAN - Press Release - VLC for iOS 3.4";
   $lang = "en";
   $menu = array( "vlc" );
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>

<h1>VLC media player for iOS, iPadOS and tvOS version 3.4</h1>
<br />

<div class="longtext">
<div class="date">Paris, May 3rd 2023</div>

<p><b>We have exciting news to share. VLC media player for iPhone, iPod touch, iPad and Apple TV will receive a major update <a href="https://itunes.apple.com/app/id650377962">on the App Store</a> today!</b></p>

<p>It will be available free of charge in any country, requires iOS 9.0 or later and runs on iPhone 4S, iPad 2nd generation, 1st generation iPad mini and Apple TV HD or any later device.</p>

<img src='/images/vlc-ios/ipad-pro-iphone-13-iphone-4s.png' alt='iPad Pro iPhone 13 iPhone 4s' class='center-block img-responsive' />

<p>Besides stability and performance improvements, this update adds support for CarPlay for iOS 14 and later, a new audio playback user interface and several new views for the audio media library including the artists and the albums.</p>

<p>This update also adds new options such as bookmarks, customization of the seek duration, a feature allowing to play videos as audio only and the display of the album track numbers in the album view. Finally, Handoff integration allows easy access to the WiFi sharing feature from nearby Macs.</p>

<p>As part of continued maintenance of the tvOS port, the Apple Remote's single click mode is now fully supported.</p>

<p>Users can now contact the team more easily via the about section of the application through an email!</p>

<img src='/images/vlc-ios/iphone13-landscape-library-black.png' alt='iPhone 13 landscape library black' class='center-block img-responsive' />

<p>VLC for iOS is fully open-source. Its code is available online and is bi-licensed under both the Mozilla Public License Version 2 as well as the GNU General Public License Version 2 or later. The MPLv2 is applicable for distribution on the App Store.</p>

<p>We are happy to answer any questions you may have</p>

<h2>Links</h2>
<p><a href="https://itunes.apple.com/app/id650377962">VLC for iOS on the App Store</a></p>

<h2>Press Contact</h2>
<p>VideoLAN Press Team, <a href="mailto:press@videolan.org">press at videolan dot org</a></p>

<h2>Pictures</h2>

<img src='/images/vlc-ios/carplay-player.jpg' alt='CarPlay audio player' class='center-block img-responsive' />
<img src='/images/vlc-ios/iphone-14pro-album-view.png' alt='iPhone 14 Pro album view' class='center-block img-responsive' />
<img src='/images/vlc-ios/iphone-14pro-audio-player.png' alt='iPhone 14 Pro audio player' class='center-block img-responsive' />

</div>

<?php footer('$Id: ios33.php 6098 2010-05-26 23:50:46Z jb $'); ?>

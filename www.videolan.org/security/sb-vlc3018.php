<?php
   $title = "VideoLAN Security Bulletin VLC 3.0.18";
   $lang = "en";
   $menu = array( "vlc" );
   $body_color = "red";
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>


<div id="fullwidth">

<h1>Security Bulletin VLC 3.0.18</h1>
<pre>
Summary           : Multiple vulnerabilities fixed in VLC media player
Date              : November 2022
Affected versions : VLC media player 3.0.17 and earlier
ID                : VideoLAN-SB-VLC-3018
</pre>

<h2>Details</h2>
<p>A denial of service could be triggered with a wrong mp4 file (div by 0) (#27202)</p>
<p>Fix crashes with multiple files due to double free (#26930)</p>
<p>A denial of service could be triggered with wrong oog file (null pointer dereference) (#27294)</p>
<p>Potential buffer overflow in the vnc module could trigger remote code execution if a malicious vnc URL is deliberately played (#27335, CVE-2022-41325)</p>

<h2>Impact</h2>
<p>If successful, a malicious third party could trigger either a crash of VLC or an arbitratry code execution with the privileges of the target user.</p>
<p>While these issues in themselves are most likely to just crash the player, we can't exclude that they could be combined to leak user informations or
remotely execute code. ASLR and DEP help reduce the likelyness of code execution, but may be bypassed.</p>
<p>We have not seen exploits performing code execution through these vulnerability</p>
<br />

<h2>Threat mitigation</h2>
<p>Exploitation of those issues requires the user to explicitly open a specially crafted file or stream.</p>

<h2>Workarounds</h2>
<p>The user should refrain from opening files from untrusted third parties
or accessing untrusted remote sites (or disable the VLC browser plugins),
until the patch is applied.
</p>

<h2>Solution</h2>
<p>VLC media player <b>3.0.18</b> addresses the issue.
</p>

<h2>Credits</h2>
<p>The vnc module vulnerability was reported and fixed by 0xMitsurugi from Synacktiv (#27335, CVE-2022-41325)</p>


<h2>References</h2>
<dl>
<dt>The VideoLAN project</dt>
<dd><a href="//www.videolan.org/">http://www.videolan.org/</a>
</dd>
<dt>VLC official GIT repository</dt>
<dd><a href="http://git.videolan.org/?p=vlc/vlc-3.0.git">http://git.videolan.org/?p=vlc.git</a>
</dd>
</dl>

</div>

<?php footer('$Id$'); ?>
